jQuery(document).ready( function() { 
    jQuery('#vs-open-search').click(function() {
        jQuery(this).toggleClass('vs-rotate-search');
        jQuery('#vs-search').toggleClass('vs-show-search');
    });
    
    jQuery('#close-wpvs-search').click(function() {
        jQuery('#vs-open-search').removeClass('vs-rotate-search');
        jQuery('#vs-search').removeClass('vs-show-search');
    });
    
    
    jQuery('#vs-search-input').keyup( function() {
        var search_for = jQuery(this).val();
        if(search_for != "") {
            jQuery('#searching-videos').show();
            jQuery('#vs-search-results').hide();
            jQuery.ajax({
                url: rvssearch.url,
                type: "POST",
                data: {
                    'action': 'rvs_search_videos',
                    'search_term': search_for
                },
                success:function(response) {
                    var vs_found = JSON.parse(response);
                    var videos = vs_found.videos;
                    var genres = vs_found.genres;
                    var actors = vs_found.actors;
                    var directors = vs_found.directors;
                    jQuery('#searching-videos').hide();
                    jQuery('#found-genres').html("");
                    jQuery('#found-actors').html("");
                    jQuery('#found-directors').html("");
                    jQuery('.found-tax').slideUp();
                    if(genres.length <= 0) {
                        jQuery('#genre-count').text('0');
                    } else {
                        var add_to_genres = "";
                        jQuery.each(genres, function(index, genre) {
                            add_to_genres += '<a href="'+genre.genre_link+'" class="vs-tax-result">'+genre.genre_title+'</a>';
                        });
                        jQuery('#found-genres').html(add_to_genres);
                        jQuery('#genre-count').text(genres.length);
                    }
                    
                    if(actors.length <= 0) {
                        jQuery('#actor-count').text('0');
                    } else {
                        var add_to_actors = "";
                        jQuery.each(actors, function(index, actor) {
                            add_to_actors += '<a href="'+actor.actor_link+'" class="vs-tax-result">'+actor.actor_title+'</a>';
                        });
                        jQuery('#found-actors').html(add_to_actors);
                        jQuery('#actor-count').text(actors.length);
                    }
                    
                    if(directors.length <= 0) {
                        jQuery('#director-count').text('0');
                    } else {
                        var add_to_directors = "";
                        jQuery.each(directors, function(index, director) {
                            add_to_directors += '<a href="'+director.director_link+'" class="vs-tax-result">'+director.director_title+'</a>';
                        });
                        jQuery('#found-directors').html(add_to_directors);
                        jQuery('#director-count').text(directors.length);
                    }


                    if(videos.length <= 0) {
                        jQuery('#vs-search-videos').html('No videos found.');
                    } else {
                        var add_to_list = "";
                        jQuery.each(videos, function(index, video) {
                            add_to_list += '<a class="video-item border-box" href="'+video.video_link+'"><div class="video-item-content"><img src="'+video.video_thumbnail+'" alt="'+video.video_title+'"/><div class="video-slide-details border-box"><h4>'+video.video_title+'</h4><p>'+video.video_excerpt+'</p></div></div></a>';
                        });
                        jQuery('#vs-search-videos').html(add_to_list);
                    }
                    jQuery('#vs-search-results').show();
                }
            });
        } else {
            jQuery('#found-genres').html("");
            jQuery('#found-actors').html("");
            jQuery('#found-directors').html("");
            jQuery('#genre-count').text('0');
            jQuery('#actor-count').text('0');
            jQuery('#director-count').text('0');
            jQuery('#vs-search-videos').html('No videos found.');
        }
    });
    jQuery('.vs-open-tax').click(function() {
        jQuery(this).next('.found-tax').slideToggle();
    });
});
    