jQuery(document).ready(function() {
    jQuery("nav#mobile .mobile-arrow").click(function () {
        jQuery(this).prev('.sub-menu').slideToggle();
        jQuery(this).toggleClass('rotateArrow');
    });
    
    jQuery("nav#mobile .userArrow").click(function () {
        jQuery('ul#user-sub-menu').slideToggle();
    });
  
    jQuery("nav#desktop .sub-arrow").click(function () {
        jQuery(this).prev('.sub-menu').slideToggle();
    });

    jQuery(window).scroll( function() {
       if(jQuery(window).scrollTop() > 50) {
           jQuery('#header').addClass('header-background');
       } else {
           jQuery('#header').removeClass('header-background');
       }
    });
    
    jQuery('#open-sub-video-cats').click(function() {
        jQuery('#select-sub-category').slideToggle();
    });
                                          
});

jQuery(window).resize(wpvs_reset_main_menu);

function wpvs_reset_main_menu() {
    if(jQuery(window).width() >= 768) {
        jQuery('ul#user-sub-menu').slideUp();
    }
}